var lodash = require('lodash');
var adapterAutoComplete = require('ancient-cursor').adapterAutoComplete;
var AncientRefsAdapterLokiJS = require('ancient-refs-adapter-lokijs');
var AncientCursorAdapterLokiJS = require('ancient-cursor-adapter-lokijs');

var wrapData = function(data) {
  if (data) return { id: ""+data.$loki, source: data.source, target: data.target, data: data }
  else return undefined;
};

module.exports = function adapterGenerator(collection) {
  return function adapterConstructor() {
    
    // (id: String, callback: (error?, result?))
    this.getRef = function(id, callback) {
      var data = collection.get(parseInt(id));
      var link = wrapData(data);
      callback(undefined, link);
    };
    
    // (data: any) => id: String
    this.newRef = function(data) {
      return data.id;
    };
    
    // (id: String, callback: (error?, result?: Boolean))
    this.deleteRef = function(id, callback) {
      this.getRef(id, function(error, data) {
        if (data && data.data) {
          collection.remove(data.data);
          callback(undefined, true);
        } else {
          callback(undefined, false);
        }
      });
    };
    
    // incompleteLinks?: Boolean = false
    incompleteLinks: true,
    
    // (link: Link, options: any, callback: (error?, id?: String))
    this.createLink = function(link, options, callback) {
      var newLink = {};
      if (link.source) newLink.source = link.source;
      if (link.target) newLink.target = link.target;
      var insertedLink = collection.insert(newLink);
      callback(undefined, ""+insertedLink.$loki);
    };
    
    // (id: String, uLink: Link, options: any, callback: (error?, count?: Number))
    this.updateLink = function(id, uLink, options, callback) {
      var data = collection.get(id);
      var count = 0;
      if (data) {
        count++;
        if (uLink.hasOwnProperty('source')) {
          if (uLink.source) data.source = uLink.source;
          else delete data.source;
        }
        if (uLink.hasOwnProperty('target')) {
          if (uLink.target) data.target = uLink.target;
          else delete data.target;
        }
        collection.update(data);
      }
      callback(undefined, count);
    };
    
    // (sLink: Link, uLink: Link, options: any, callback: (error?, count?: Number))
    this.updateLinks = function(sLink, uLink, options, callback) {
      var query = {};
      var count = 0;
      if (sLink.hasOwnProperty('source')) query.source = sLink.source;
      if (sLink.hasOwnProperty('target')) query.target = sLink.target;
      var results = collection.find(query);
      for (var r in results) {
        count++;
        if (uLink.hasOwnProperty('source')) results[r].source = uLink.source;
        if (uLink.hasOwnProperty('target')) results[r].target = uLink.target;
        collection.update(results[r]);
      }
      callback(undefined, count);
    };
    
    // (sLink: Link, options: any, callback: (error?, count?: Number))
    this.deleteLinks = function(sLink, options, callback) {
      var query = {};
      var count = 0;
      if (sLink.hasOwnProperty('source')) query.source = sLink.source;
      if (sLink.hasOwnProperty('target')) query.target = sLink.target;
      var results = collection.find(query);
      for (var r in results) {
        count++;
        collection.remove(results[r]);
      }
      callback(undefined, count);
    };
    
    // (sLink: Link) => cursorPointer: any
    this.searchLinks = function(sLink) {
      var query = {};
      if (sLink.hasOwnProperty('source')) query.source = sLink.source;
      if (sLink.hasOwnProperty('target')) query.target = sLink.target;
      return query;
    }
    
    // (pointer: any, callback: (error?, results?: [any])
    this.cursorFetch = function(pointer, callback) {
      var datas = collection.find(pointer);
      var results = [];
      for (var d in datas) {
        results.push(wrapData(datas[d]));
      }
      callback(undefined, results);
    };
    
    // (pointer: any, handler: (error?, results?: [any], callback: ())
    this.cursorEach = function(pointer, handler, callback) {
      this.cursorFetch(pointer, function(error, results) {
        if (error) callback(error);
        else {
          for (var r in results) {
            handler(undefined, results[r]);
          }
          callback();
        }
      });
    };
    
    // (pointer: any, callback: (error?, count?: Number)
    this.cursorCount = function(pointer, callback) {
      this.cursorFetch(pointer, function(error, results) {
        callback(error, results?results.length:0);
      });
    };
    
    // (pointer: any, begin: Number, length: Number, callback: (error?, results?: [any])
    this.cursorSlice = function(pointer, begin, length, callback) {
      this.cursorFetch(pointer, function(error, _results) {
        var results;
        if (_results) results = _results.slice(begin, begin+length);
        callback(error, results);
      });
    };
    
    // (pointer: any, callback(error?, result? any)
    this.cursorFirst = function(pointer, callback) {
      callback(undefined, wrapData(collection.findOne(pointer)));
    };
    
    return this;
  };
};