var Loki = require('lokijs');

var Adapters = require('ancient-adapters').Adapters;
var Cursor = require('ancient-cursor');
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Graph = require('ancient-graph');
var AncientGraphAdapterLokiJS = require('../index.js');
var TestAncientRefs = require('ancient-refs/tests/test.js');

var testGraphAdapter = require('ancient-graph/tests/testAdapter.js');
var testGraph = require('ancient-graph/tests/testGraph.js');

describe('ancient-graph-adapter-lokijs', function() {
  var db = new Loki('db');
  
  var adapters = new Adapters();
  new DocumentsFactory(adapters);
  new Refs(adapters.documentFactory);
  adapters.addClass('graph', Graph.adaptersAddClass);
  
  (function() {
    var collection = db.addCollection('graph');
    adapters.add('graph', AncientGraphAdapterLokiJS(collection));
  })();
  
  (function() {
    var collection = db.addCollection('dots');
    adapters.add('dots', AncientGraphAdapterLokiJS(collection));
  })();
  
  (function() {
    var collection = db.addCollection('dots');
    adapters.add('refs', AncientGraphAdapterLokiJS(collection));
    
    collection.insert({});
    collection.insert({});
    collection.insert({});
    
    var results = collection.find();
    var requiredResults = {};
    
    for (var r in results) {
      requiredResults['refs' + '/' + results[r].$loki] = {
        id: ""+results[r].$loki, data: results[r],
        source: undefined, target: undefined
      };
    }
    
    describe('test Refs adapter', function() {
      TestAncientRefs(Refs, adapters.documentFactory, 'refs', requiredResults, true, true);
    });
  })();
  
  describe('test Graph adapter LokiJS', function() {
    testGraphAdapter(adapters, 'graph', 'dots');
  });
  
  describe('test Graph on adapter LokiJS', function() {
    var graph = adapters.get('graph').asClass('graph');
    testGraph(graph, adapters, 'dots');
  });
});