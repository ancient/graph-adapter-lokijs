# GraphAdapterLokiJS^0.0.0 ^[wiki](https://gitlab.com/ancient/graph-adapter-lokijs/wikis/home)

Adapter based on [LokiJS](http://lokijs.org/#/) for [ancient-graph](https://gitlab.com/ancient/graph) package.

## Example

```js
var loki = require('lokijs');

var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Cursor = require('ancient-cursor').Cursor;
var AncientGraphAdapterLokiJS = require('ancient-graph-adapter-lokijs');

var db = new loki('db');

var adapters = new Adapters();
adapters.addClass('graph', Graph.adaptersAddClass);

adapters.add('graph', AncientGraphAdapterLokiJS(db.addCollection('graph')));
adapters.add('dots', AncientGraphAdapterLokiJS(db.addCollection('dots')));

var documentsFactory = new DocumentsFactory(adapters);
var refs = new Refs(documentsFactory);

var graph = adapters.get('graph').asClass('graph');
var dots = adapters.get('dots').asClass('graph');

var dotId = dots.create({});
var linkId = graph.create({ source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId });

var cursor = graph.search({ source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId });
cursor.first();
// Document { data: { id: linkId, source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId, data: { $loki: linkId } } }

graph.update({ target: dots.adapterName+'/'+dotId }, { source: graph.adapterName+'/'+linkId });

graph.search({ source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId }).first();
// Document { data: { id: linkId, source: graph.adapterName+'/'+linkId, target: dots.adapterName+'/'+dotId, data: { $loki: linkId } } }

var cursor = graph.search({});
cursor.count();
// 1
cursor.fetch();
// [Document { data: { id: linkId, source: graph.adapterName+'/'+linkId, target: dots.adapterName+'/'+dotId, data: { $loki: linkId } } }]
graph.delete({});
// 1
cursor.count();
// 0
```